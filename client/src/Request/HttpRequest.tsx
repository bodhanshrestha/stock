import axios from 'axios';

axios.interceptors.request.use(
	(request) => {
		const accessToken = localStorage.getItem('token');
		const headers = {
			'Content-Type': 'application/json',
			Authorization: `Bearer ${accessToken}`,
		};
		request.headers = headers;
		return request;
	},
	(error) => {
		return Promise.reject(error);
	}
);

const GET = async (url: string) => {
	return await axios.get(url);
};
const POST = async (url: string, data: any) => {
	return await axios.post(url, data);
};

const PUT = async (url: string, data: any) => {
	return await axios.put(url, data);
};

const DELETE = async (url: string) => {
	return await axios.delete(url);
};

const HttpRequest = {
	GET,
	POST,
	PUT,
	DELETE,
};

export default HttpRequest;
