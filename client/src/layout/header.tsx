import React from 'react';
import { Button, Layout } from 'antd';
import { useHistory } from 'react-router';

const { Header } = Layout;
const HeaderComponent = () => {
	const history = useHistory();
	const handleLogout = () => {
		localStorage.clear();
		history.push('/');
	};
	return (
		<Header className='flex-right'>
			<Button onClick={handleLogout}>Logout</Button>
		</Header>
	);
};

export default HeaderComponent;
