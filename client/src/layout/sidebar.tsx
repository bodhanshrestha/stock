import React from 'react';
import { Layout, Menu } from 'antd';

import { useHistory } from 'react-router';
import { Link } from 'react-router-dom';
import { DashboardOutlined, MoneyCollectOutlined } from '@ant-design/icons';
const { Sider } = Layout;

const Sidebar = () => {
	const history = useHistory();
	const path: any = history.location.pathname;
	return (
		<Sider width={200} theme='light' className='full-menu'>
			<Menu
				mode='inline'
				defaultSelectedKeys={['/dashboard']}
				selectedKeys={path}>
				<Menu.Item key='/dashboard' icon={<DashboardOutlined />}>
					<Link to='/dashboard'>Dashboard</Link>
				</Menu.Item>
				<Menu.Item key='/investment' icon={<MoneyCollectOutlined />}>
					<Link to='/investment'>Investment</Link>
				</Menu.Item>
			</Menu>
		</Sider>
	);
};

export default Sidebar;
