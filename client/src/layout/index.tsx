import React from 'react';
import { Layout } from 'antd';
import HeaderComponent from './header';
import './index.css';
import Sidebar from './sidebar';
const { Content } = Layout;
const LayoutComponent = ({ children }: any) => {
	return (
		<Layout>
			<HeaderComponent />
			<Layout>
				<Sidebar />
				<Content style={{ padding: '20px 40px' }}>{children}</Content>
			</Layout>
		</Layout>
	);
};

export default LayoutComponent;
