import React from 'react';
import { Layout } from 'antd';
import HeaderComponent from './header';
import './index.css';
const { Content } = Layout;
const AdminLayout = ({ children }: any) => {
	return (
		<Layout>
			<HeaderComponent />
			<Content style={{ padding: '20px 40px' }}>{children}</Content>
		</Layout>
	);
};

export default AdminLayout;
