import React from 'react';
import { Form, Button } from 'antd';
import {
	FormEmailItem,
	FormInputItem,
	FormInputPasswordItem,
} from '../../component/FormInput';
import { Link } from 'react-router-dom';

const LoginForm = () => {
	return (
		<>
			<FormInputItem
				label='Name'
				name='name'
				required={true}
				placeholder='Enter Your Full Name'
				message='Please input your full name!'
			/>
			<FormInputItem
				label='Username'
				name='username'
				required={true}
				placeholder='Enter Your Username'
				message='Please input your username!'
			/>
			<FormEmailItem
				label='Email'
				name='email'
				placeholder='Enter Your Email'
			/>
			<FormInputPasswordItem
				label='Password'
				name='password'
				required={true}
				placeholder='Enter Your Password'
				message='Please input your password!'
			/>
			<FormInputItem
				label='Address'
				name='address'
				required={true}
				placeholder='Enter Your Address'
				message='Please input your address!'
			/>
			<FormInputItem
				label='Phone Number'
				name='phone_number'
				required={true}
				placeholder='Enter Your Phone Number'
				message='Please input your phone number!'
				type='number'
			/>

			<Form.Item>
				<Button type='primary' htmlType='submit' style={{ width: '100%' }}>
					Register
				</Button>
			</Form.Item>

			<span>
				<Link to='/'>Login</Link>
			</span>
		</>
	);
};

export default LoginForm;
