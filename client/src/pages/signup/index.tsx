import React from 'react';
import { Form, message } from 'antd';
import SignUpForm from './form';
import { RegisterRequest } from '../../services';
import { ErrorMessage } from '../../component/errorMessage';
import { useHistory } from 'react-router';
const SignUpPage = () => {
	const history = useHistory();
	const onFinish = (values: any) => {
		RegisterRequest(values)
			.then((res) => {
				message.success(res.data.message);
				history.push('/');
			})
			.catch((err) => {
				ErrorMessage(err);
			});
	};

	return (
		<div className='signup'>
			<Form
				name='signUp'
				layout='vertical'
				onFinish={onFinish}
				autoComplete='off'>
				<SignUpForm />
			</Form>
		</div>
	);
};

export default SignUpPage;
