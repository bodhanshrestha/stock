import React, { Fragment } from 'react';
import Indicator from './indicator';
import { Divider, Typography } from 'antd';

const IndividualList = ({ data }: any) => {
	return (
		<div>
			{data &&
				data.map((el: any, i: number) => (
					<Fragment key={i}>
						<Typography.Title level={3}>{el.name}</Typography.Title>
						<Indicator data={el.data} />
						<Divider />
					</Fragment>
				))}
		</div>
	);
};

export default IndividualList;
