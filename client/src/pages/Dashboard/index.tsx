import React, { useEffect, useState } from 'react';
import AvailableStock from './availableStock';
import { Row, Col, Divider } from 'antd';
import Indicator from './indicator';
import { GetDashboardShowData, GetDashboardShowListData } from '../../services';
import { ErrorMessage } from '../../component/errorMessage';
import IndividualList from './individual';
const Dashboard = () => {
	const [data, setData] = useState([]);
	const [showList, setShowList] = useState([]);
	const getShowData = () => {
		GetDashboardShowData()
			.then((res) => setData(res.data))
			.catch((e) => ErrorMessage(e));
	};
	const getShowListData = () => {
		GetDashboardShowListData()
			.then((res) => setShowList(res.data))
			.catch((e) => ErrorMessage(e));
	};
	useEffect(() => {
		getShowData();
		getShowListData();
		return () => {};
	}, []);
	return (
		<div>
			<Row gutter={[10, 10]}>
				<Col span={18}>
					<Indicator data={data} />
				</Col>
				<Col span={6}>
					<AvailableStock />
				</Col>
			</Row>
			<Divider />
			<IndividualList data={showList} />
		</div>
	);
};

export default Dashboard;
