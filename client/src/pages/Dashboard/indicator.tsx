import React, { Fragment } from 'react';
import { Row, Col } from 'antd';
import CardComponent from '../../component/card';
const Indicator = ({ data }: any) => {
	return (
		<Row gutter={[16, 16]}>
			{data &&
				data.map((el: any, i: number) => (
					<Fragment key={i}>
						{el.total !== undefined && (
							<Col>
								<CardComponent title={el.title} total={el.total} />
							</Col>
						)}
					</Fragment>
				))}
		</Row>
	);
};

export default Indicator;
