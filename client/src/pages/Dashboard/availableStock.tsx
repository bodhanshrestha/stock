import React, { useState, useEffect } from 'react';
import { Table } from 'antd';
import { ListAllStock } from '../../services';

const AvailableStock = () => {
	const [stock, setStock] = useState([]);
	const columns = [
		{
			title: <b>Available Stocks</b>,
			dataIndex: 'name',
			key: 'name',
		},
	];
	const getStockData = () => {
		ListAllStock().then((res) => {
			setStock(res.data);
		});
	};
	useEffect(() => {
		getStockData();
		return () => {};
	}, []);
	return (
		<Table
			columns={columns}
			dataSource={stock}
			pagination={false}
			rowKey={(record: any) => record._id}
		/>
	);
};

export default AvailableStock;
