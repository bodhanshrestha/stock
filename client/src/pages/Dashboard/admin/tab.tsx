import React from 'react';
import { Tabs } from 'antd';
import AdminTable from './table';
import AdminFrom from './form';

const { TabPane } = Tabs;

const TabComponent = () => {
	const [activeTabKey, setActiveTabKey] = React.useState('1');
	const [selectedData, setSelectedData] = React.useState({});
	const [id, setId] = React.useState<string>('');

	const changeTab = (activeKey: string) => {
		setActiveTabKey(activeKey);
	};
	const onCancel = () => {
		setActiveTabKey('1');
		setId('');
		setSelectedData({});
	};
	return (
		<Tabs
			defaultActiveKey='1'
			destroyInactiveTabPane={true}
			activeKey={activeTabKey}
			onChange={changeTab}>
			<TabPane tab='List All Stock' key='1'>
				<AdminTable
					setActiveTabKey={setActiveTabKey}
					setSelectedData={setSelectedData}
					setId={setId}
				/>
			</TabPane>
			<TabPane
				tab={`${selectedData && id ? 'Update Stock' : 'Add New Stock'}`}
				key='2'>
				<div className='tab_admin'>
					<AdminFrom
						selectedData={selectedData}
						selectedId={id}
						onCancel={onCancel}
					/>
				</div>
			</TabPane>
		</Tabs>
	);
};

export default TabComponent;
