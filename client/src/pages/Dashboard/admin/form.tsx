import React, { useEffect } from 'react';
import { FormInputItem } from '../../../component/FormInput';
import { Form, Button, message } from 'antd';
import { AddStock, UpdateStock } from '../../../services';
import { ErrorMessage } from '../../../component/errorMessage';
const AdminFrom = ({ selectedData, selectedId, onCancel }: any) => {
	const [form] = Form.useForm();
	const handleAdd = (data: any) => {
		AddStock(data)
			.then((res) => {
				message.success(res.data.message);
			})
			.catch((e) => ErrorMessage(e));
	};
	const handleUpdate = (id: string, data: any) => {
		UpdateStock(id, data)
			.then((res) => {
				message.success(res.data.message);
				onCancel();
			})
			.catch((e) => ErrorMessage(e));
	};
	const onFinish = (values: any) => {
		form.resetFields();
		if (selectedData && selectedId) {
			return handleUpdate(selectedId, values);
		}
		return handleAdd(values);
	};

	useEffect(() => {
		if (selectedData) {
			form.setFieldsValue(selectedData);
		}
		return () => {};
	}, [selectedData, form]);
	return (
		<Form form={form} name='login' layout='vertical' onFinish={onFinish}>
			<FormInputItem
				label='Name'
				name='name'
				required={true}
				placeholder='Enter Stock Name'
				message='Please input stock name!'
			/>

			<div
				style={{
					display: 'flex',
					justifyContent: 'center',
					alignContent: 'center',
					gap: '20px',
				}}>
				<Form.Item style={{ width: '100%' }}>
					<Button type='primary' htmlType='submit' style={{ width: '100%' }}>
						{selectedData && selectedId ? 'Update' : 'Add'}
					</Button>
				</Form.Item>
				<Form.Item style={{ width: '100%' }}>
					{selectedData && selectedId ? (
						<Button
							type='default'
							htmlType='button'
							style={{ width: '100%' }}
							onClick={onCancel}>
							Cancel
						</Button>
					) : (
						<Button type='default' htmlType='reset' style={{ width: '100%' }}>
							Reset
						</Button>
					)}
				</Form.Item>
			</div>
		</Form>
	);
};

export default AdminFrom;
