import React, { useEffect, useState } from 'react';
import { Table, Popconfirm, message } from 'antd';
import { DeleteStock, ListAllStock } from '../../../services';
import { DeleteOutlined, EditOutlined } from '@ant-design/icons';
import { ErrorMessage } from '../../../component/errorMessage';

const AdminTable = ({ setActiveTabKey, setSelectedData, setId }: any) => {
	const [stock, setStock] = useState([]);
	const columns = [
		{
			title: 'Name',
			dataIndex: 'name',
			key: 'name',
		},

		{
			title: 'Action',
			key: 'action',
			width: '100px',
			render: (_: any, record: any) => (
				<div style={{ display: 'flex', justifyContent: 'center', gap: '20px' }}>
					<div style={{ cursor: 'pointer' }}>
						<EditOutlined
							onClick={() => {
								setId(record._id);
								setSelectedData(record);
								setActiveTabKey('2');
							}}
						/>
					</div>
					<div style={{ cursor: 'pointer' }}>
						<Popconfirm
							title={`Are you sure you want to delete ${record.name}?`}
							onConfirm={() => handleDelete(record._id)}
							className='delete_button'
							okText='Yes'
							cancelText='No'>
							<DeleteOutlined />
						</Popconfirm>
					</div>
				</div>
			),
		},
	];
	const handleDelete = (id: string) => {
		DeleteStock(id)
			.then((res) => {
				getStockData();
				message.success(res.data.message);
			})
			.catch((e) => ErrorMessage(e));
	};
	const getStockData = () => {
		ListAllStock().then((res) => {
			setStock(res.data);
		});
	};
	useEffect(() => {
		getStockData();
		return () => {};
	}, []);
	return (
		<Table
			columns={columns}
			dataSource={stock}
			pagination={false}
			rowKey={(record) => record._id}
		/>
	);
};

export default AdminTable;
