export type investedFormProps = {

  stock: any[],
  handleAdd: (values: any) => void
}
export type onFinishInvestedProps = {
  stockId: string,
  quantity: number
}

export type StockProps = () => {
  stock?: string,
  stockId?: string,
  name?: string,
  transaction_type?: string,
  quantity?: number,
  amount?: number,
  transaction_date?: string,
}