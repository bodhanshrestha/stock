import React, { useEffect, useState } from 'react';
import { Table } from 'antd';

import { ListAllInvested } from '../../services';
import { ConvertMomentToDate } from '../../util/date';

const InvestedTable = ({ listenChange }: { listenChange: boolean }) => {
	const [Invested, setInvested] = useState([]);
	const columns: any = [
		{
			title: 'Stock Name',
			dataIndex: ['stock', 'name'],
			key: 'stock',
			sorter: (a: any, b: any) => a.stock.name.localeCompare(b.stock.name),
			sortDirections: ['descend', 'ascend'],
		},
		{
			title: 'Transaction Type',
			dataIndex: 'transaction_type',
			key: 'transaction_type',
			render: (data: string) => <p>{data === 'buy' ? 'Buy' : 'Sell'}</p>,
			sorter: (a: any, b: any) =>
				a.transaction_type.localeCompare(b.transaction_type),
			sortDirections: ['descend', 'ascend'],
		},
		{
			title: 'Quantity',
			dataIndex: 'quantity',
			key: 'quantity',
		},
		{
			title: 'Total Amount',
			dataIndex: 'amount',
			key: 'amount',
		},
		{
			title: 'Transaction Date',
			dataIndex: 'transaction_date',
			key: 'transaction_date',
			render: (date: string) => <p>{ConvertMomentToDate(date)}</p>,
			sorter: (a: any, b: any) =>
				a.transaction_date.localeCompare(b.transaction_date),
			sortDirections: ['descend', 'ascend'],
		},
	];

	const getInvestedData = () => {
		ListAllInvested().then((res) => {
			setInvested(res.data);
		});
	};
	useEffect(() => {
		getInvestedData();
		return () => {};
	}, [listenChange]);
	return (
		<Table
			columns={columns}
			dataSource={Invested}
			pagination={false}
			rowKey={(record: any) => record._id}
		/>
	);
};

export default InvestedTable;
