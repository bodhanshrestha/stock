import React, { useState } from 'react';
import OptionForInvestment from './option';
import InvestedTable from './table';
import { Row, Col } from 'antd';
import './index.css';
const InvestedPage = () => {
	const [listenChange, setListenChange] = useState(false);
	return (
		<div>
			<Row gutter={[16, 16]}>
				<Col span={8}>
					<OptionForInvestment setListenChange={setListenChange} />
				</Col>

				<Col span={14}>
					<InvestedTable listenChange={listenChange} />
				</Col>
			</Row>
		</div>
	);
};

export default InvestedPage;
