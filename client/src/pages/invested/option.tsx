import React, { useEffect, useState } from 'react';
import { message } from 'antd';
import { ErrorMessage } from '../../component/errorMessage';
import { BuyStock, ListAllStock } from '../../services';
import InvestedForm from './form';
import { StockProps } from './types';

const OptionForInvestment = ({ setListenChange }: any) => {
	const [stock, setStock] = useState([]);

	const getStockData = () => {
		ListAllStock().then((res) => {
			setStock(res.data);
		});
	};

	const handleAdd = (incomingData: StockProps) => {
		const { stock, transaction_type, quantity, amount, transaction_date }: any =
			incomingData;
		const totalAmount = Number(quantity) * Number(amount);
		const data = {
			stock,
			transaction_type,
			quantity,
			amount: totalAmount,
			transaction_date,
		};
		BuyStock(data)
			.then((res) => {
				setListenChange(true);
				message.success(res.data.message);
			})
			.catch((e) => ErrorMessage(e));
	};

	useEffect(() => {
		getStockData();
		return () => {};
	}, []);

	return <InvestedForm stock={stock} handleAdd={handleAdd} />;
};

export default OptionForInvestment;
