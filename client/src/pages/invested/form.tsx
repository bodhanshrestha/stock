import React, { useEffect, useState } from 'react';

import { Form, Button, DatePicker } from 'antd';
import { FormInputItem } from '../../component/FormInput';
import { FormSelectItem } from '../../component/FormSelect';
import { investedFormProps, onFinishInvestedProps } from './types';
import { GetInvestedQuantity } from '../../services';
import { ErrorMessage } from '../../component/errorMessage';

const InvestedForm = ({ stock, handleAdd }: investedFormProps) => {
	const [form] = Form.useForm();
	const [stockId, setStockId] = useState('');
	const [type, setType] = useState('');
	const [quantity, setQuantity] = useState<number>();
	const onFinish = (values: onFinishInvestedProps) => {
		handleAdd(values);
		setQuantity(undefined);
		form.resetFields();
	};
	const getData = () => {
		GetInvestedQuantity(stockId)
			.then((res) => {
				setQuantity(res.data);
			})
			.catch((e) => {
				ErrorMessage(e);
			});
	};
	useEffect(() => {
		if (stockId && type === 'sell') {
			return getData();
		} else {
			setQuantity(undefined);
		}
		// eslint-disable-next-line react-hooks/exhaustive-deps
	}, [type, stockId]);
	return (
		<Form
			form={form}
			name='login'
			layout='vertical'
			onFinish={onFinish}
			className='invested-form'>
			<FormSelectItem
				label='Stock Name'
				name='stock'
				required={true}
				placeholder='Select Stock Name'
				message='Please select stock name!'
				data={stock}
				callback={(e: string) => setStockId(e)}
			/>
			<FormSelectItem
				label='Transaction type'
				name='transaction_type'
				required={true}
				message='Please select transaction type!'
				placeholder='Select Transaction type'
				data={[
					{ _id: 'buy', name: 'Buy' },
					{ _id: 'sell', name: 'Sell' },
				]}
				callback={(e: string) => setType(e)}
			/>

			<FormInputItem
				label={`${
					quantity === 0
						? 'Quantity (Out of Stock)'
						: quantity
						? `Quantity (Available Stock is ${quantity})`
						: 'Quantity'
				}`}
				name='quantity'
				required={true}
				placeholder='Enter Stock quantity'
				message='Please input stock quantity!'
				type='number'
				min={1}
				max={quantity}
			/>
			<FormInputItem
				label='Amount (Per Unit)'
				name='amount'
				required={true}
				placeholder='Enter Stock Amount'
				message='Please input stock Amount!'
				type='number'
				min={1}
			/>
			<Form.Item
				label='Transaction Date'
				name='transaction_date'
				rules={[{ required: true, message: 'Select Transaction Date' }]}>
				<DatePicker />
			</Form.Item>

			<div
				style={{
					display: 'flex',
					justifyContent: 'center',
					alignContent: 'center',
					gap: '20px',
				}}>
				<Form.Item style={{ width: '100%' }}>
					<Button type='primary' htmlType='submit' style={{ width: '100%' }}>
						Add
					</Button>
				</Form.Item>
				<Form.Item style={{ width: '100%' }}>
					<Button
						type='default'
						htmlType='reset'
						style={{ width: '100%' }}
						onClick={() => setQuantity(undefined)}>
						Reset
					</Button>
				</Form.Item>
			</div>
		</Form>
	);
};

export default InvestedForm;
