import React from 'react';
import { Form, Button } from 'antd';
import {
	FormInputItem,
	FormInputPasswordItem,
} from '../../component/FormInput';

const LoginForm = () => {
	return (
		<>
			<FormInputItem
				label='Username'
				name='username'
				required={true}
				placeholder='Enter Your Username'
				message='Please input your username!'
			/>
			<FormInputPasswordItem
				label='Password'
				name='password'
				required={true}
				placeholder='Enter Your Password'
				message='Please input your password!'
			/>

			<Form.Item>
				<Button type='primary' htmlType='submit' style={{ width: '100%' }}>
					Login
				</Button>
			</Form.Item>
		</>
	);
};

export default LoginForm;
