import React from 'react';
import { Form, message } from 'antd';
import LoginForm from './form';
import { LoginRequest } from '../../services';
import { ErrorMessage } from '../../component/errorMessage';
import './index.css';
import { Link, useHistory } from 'react-router-dom';
const LoginPage = () => {
	const history = useHistory();
	const onFinish = (values: any) => {
		LoginRequest(values)
			.then((res) => {
				localStorage.setItem('token', res.data.token);
				localStorage.setItem('username', res.data.user.username);
				localStorage.setItem('role', res.data.user.role);
				if (res.data.user.role === 'admin') {
					history.push('/adminDashboard');
				} else {
					history.push('/dashboard');
				}
				message.success(res.data.message);
			})
			.catch((err) => {
				ErrorMessage(err);
			});
	};

	return (
		<div className='login'>
			<Form
				name='login'
				layout='vertical'
				onFinish={onFinish}
				autoComplete='off'>
				<LoginForm />
			</Form>
			<span>
				<Link to='/register'>Register</Link>
			</span>
		</div>
	);
};

export default LoginPage;
