import Moment from 'moment';

export const ConvertMomentToDate = (date: any) =>
  Moment(date).format('ddd, MMM D YYYY');
