import HttpRequest from "../Request/HttpRequest"
const USER_URL = '/api/user'
const STOCK_URL = '/api/stock'
const INVESTED_URL = '/api/invested'
const DASHBOARD_URL = '/api/dashboard'
export const LoginRequest = async (data: any) => {
  return await HttpRequest.POST(`${USER_URL}/login`, data)
}
export const RegisterRequest = async (data: any) => {
  return await HttpRequest.POST(`${USER_URL}/register`, data)
}
export const AddStock = async (data: any) => {
  return await HttpRequest.POST(`${STOCK_URL}/create`, data)
}
export const UpdateStock = async (id: string, data: any) => {
  return await HttpRequest.PUT(`${STOCK_URL}/${id}/update`, data)
}
export const ListAllStock = async () => {
  return await HttpRequest.GET(`${STOCK_URL}/list`)
}
export const DeleteStock = async (id: string) => {
  return await HttpRequest.DELETE(`${STOCK_URL}/${id}/delete`)
}
export const ListAllInvested = async () => {
  return await HttpRequest.GET(`${INVESTED_URL}/list`)
}
export const GetInvestedQuantity = async (id: string) => {
  return await HttpRequest.GET(`${INVESTED_URL}/get/quantity?id=${id}`)
}
export const BuyStock = async (data: any) => {
  return await HttpRequest.POST(`${INVESTED_URL}/create`, data)
}

export const GetDashboardShowData = async () => {
  return await HttpRequest.GET(`${DASHBOARD_URL}/show`)
}
export const GetDashboardShowListData = async () => {
  return await HttpRequest.GET(`${DASHBOARD_URL}/list`)
}