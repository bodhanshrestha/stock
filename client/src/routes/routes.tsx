import React from 'react';
import { Switch, Route } from 'react-router-dom';
import Dashboard from '../pages/Dashboard';
import AdminDashboard from '../pages/Dashboard/admin';
import InvestedPage from '../pages/invested';
import LoginPage from '../pages/login';
import SignUpPage from '../pages/signup';
import AdminRoute from './admin';
import PrivateRoute from './private';
const Routes = () => {
	return (
		<Switch>
			<Route exact path='/' component={LoginPage} />
			<Route exact path='/register' component={SignUpPage} />
			<AdminRoute exact path='/adminDashboard' component={AdminDashboard} />
			<PrivateRoute exact path='/dashboard' component={Dashboard} />
			<PrivateRoute exact path='/investment' component={InvestedPage} />
		</Switch>
	);
};

export default Routes;
