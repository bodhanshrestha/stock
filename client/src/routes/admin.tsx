import React from 'react';
import { Route, Redirect } from 'react-router-dom';
import AdminLayout from '../layout/admin';

const AdminRoute = ({ component: Component, ...rest }: any) => {
	const token = localStorage.getItem('token');
	const username = localStorage.getItem('username');
	const role = localStorage.getItem('role');
	return (
		<Route
			{...rest}
			render={(props) => {
				return username && token && role === 'admin' ? (
					<AdminLayout>
						<Component {...props} />
					</AdminLayout>
				) : (
					<Redirect to='/' />
				);
			}}
		/>
	);
};

export default AdminRoute;
