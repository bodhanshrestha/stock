import React from 'react';
import { Route, Redirect } from 'react-router-dom';
import LayoutComponent from '../layout';

const PrivateRoute = ({ component: Component, ...rest }: any) => {
	const token = localStorage.getItem('token');
	const username = localStorage.getItem('username');
	return (
		<Route
			{...rest}
			render={(props) =>
				username && token ? (
					<LayoutComponent>
						<Component {...props} />
					</LayoutComponent>
				) : (
					<Redirect to='/' />
				)
			}
		/>
	);
};

export default PrivateRoute;
