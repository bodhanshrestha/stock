export type FormSelectItemComponentProps = {
  label?: string;
  name: string;
  placeholder?: string;
  message?: string;
  data: any[];
  value?: any;
  required?: boolean;
  callback?: (value: string) => void;
};
