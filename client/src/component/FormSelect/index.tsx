import React from 'react';
import { Form, Select } from 'antd';
import { FormSelectItemComponentProps } from './types';
const { Option } = Select;

export const FormSelectItem: React.FC<FormSelectItemComponentProps> = ({
	label,
	name,
	placeholder,
	message,
	data,
	required,
	callback,
}) => {
	return (
		<Form.Item
			label={label}
			name={name}
			rules={[{ required: required ? true : false, message: message }]}>
			<Select
				showSearch
				optionFilterProp='children'
				placeholder={placeholder}
				onSelect={callback}
				getPopupContainer={(trigger) => trigger.parentElement}>
				{data &&
					data.map((data: any, i: number) => (
						<Option value={data._id} key={i}>
							{data.name}
						</Option>
					))}
			</Select>
		</Form.Item>
	);
};
