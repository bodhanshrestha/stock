import { message } from 'antd';

export const ErrorMessage = (err: any) =>
	message.error(
		err.response.data.message
			? err.response.data.message
			: err.response.statusText
	);
