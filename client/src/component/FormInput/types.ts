
export type FormInputItemProps = {
  label?: string;
  name: string;
  placeholder?: string;
  value?: string | number;
  disable?: boolean;
  required?: boolean;
  callback?: (event: any) => void;
  className?: string;
  message?: string;
  type?: string;
  defaultValue?: any,
  min?: number
  max?: number
};