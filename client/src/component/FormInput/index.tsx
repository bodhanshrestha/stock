import React from 'react';
import { Form, Input } from 'antd';
import { FormInputItemProps } from './types';

export const FormInputItem: React.FC<FormInputItemProps> = ({
	label,
	name,
	placeholder,
	value,
	disable,
	required,
	message,
	callback,
	defaultValue,
	min,
	max,
	type,
}) => {
	return (
		<Form.Item
			label={label}
			name={name}
			rules={[{ required: required ? true : false, message: message }]}>
			<Input
				defaultValue={defaultValue}
				value={value ? value : ''}
				placeholder={placeholder}
				disabled={disable ? disable : false}
				onChange={callback}
				type={type ? type : 'string'}
				min={min}
				max={max}
			/>
		</Form.Item>
	);
};

export const FormInputPasswordItem: React.FC<FormInputItemProps> = ({
	label,
	name,
	placeholder,
	message,
}) => {
	return (
		<Form.Item
			label={label}
			name={name}
			rules={[
				{
					required: true,

					message: message,
				},
			]}>
			<Input.Password placeholder={placeholder} />
		</Form.Item>
	);
};

export const FormEmailItem: React.FC<FormInputItemProps> = ({
	name,
	label,
	placeholder,
}) => {
	return (
		<Form.Item
			name={name}
			label={label}
			rules={[
				{
					type: 'email',
					required: true,
					message: 'Please Enter Valid Email Address',
				},
			]}>
			<Input placeholder={placeholder} />
		</Form.Item>
	);
};
