import React from 'react';
import { Card } from 'antd';

const CardComponent = ({ title, total }: any) => {
	return (
		<Card title={<b>{title}</b>} bordered={true} style={{ width: 300 }}>
			<h1>{total}</h1>
		</Card>
	);
};

export default CardComponent;
