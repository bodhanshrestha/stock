
## For Setup Environment Variable

```
NODE_ENV=
PORT=5000
DATABASE= (Like = mongodb://localhost:27017/stock)
JWT_EXPIRE_DATE=
ACCESS_TOKEN_SECRET=

```
## Requirement for generating Admin User

1. Create a user from Register Page.
2. Change the Role of the user form 'member' to 'admin' from DATABASE.
3. Then Login to Add New Stock Name.
