import express from 'express'
import connectDB from './src/database/db'
import dotenv from 'dotenv'
import cors from 'cors';
import userRoutes from './src/module/user/router'
import stockRoutes from './src/module/stock/router'
import investedRoutes from './src/module/invested/router'
import dashboardRoutes from './src/module/dashboard/router'
import logger from 'morgan'
import { ErrorHandler } from './src/middleware/errorHandler'

dotenv.config()
connectDB()


const app = express()

app.use(cors())
app.use(logger('dev'))
app.use(express.json())
app.use(express.urlencoded({ extended: true }))

//Creating API for user
app.use('/api/user', userRoutes)
app.use('/api/stock', stockRoutes)
app.use('/api/invested', investedRoutes)
app.use('/api/dashboard', dashboardRoutes)
app.use(ErrorHandler)

const PORT = process.env.PORT || 5000

app.listen(PORT, () => console.log(`App is running in ${process.env.NODE_ENV} mode on port ${PORT}`))
