import { authenticateToken } from './../../middleware/authenticateToken';
import express from 'express'
import { show, list } from './controller'
const router = express.Router()



router.route('/show').get(authenticateToken, show)

router.route('/list').get(authenticateToken, list)



export default router