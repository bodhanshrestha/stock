import { IInvested } from './../invested/interface';
import InvestedModel from '../invested/model';
import StockModel from '../stock/model';
import UserModel from '../user/model';

export const ReduceData = (data: any) => {
  return data.reduce((acc: any, item: any) => {
    acc.quantity = acc.quantity + item.quantity
    acc.amount = acc.amount + item.amount
    return acc
  })
}

const FilterData = async (data: any, userId: string, type: string) => {
  const userData = await UserModel.findOne({ _id: userId })
  const total_investment = data.filter((el: IInvested) => el.transaction_type === 'buy')
  const total_sold = data.filter((el: IInvested) => el.transaction_type === 'sell')
  let profit
  let loss
  if (total_investment.length > 0 && total_sold.length > 0) {
    let { amount: totalInvestment, quantity: totalUnits } = ReduceData(total_investment)
    const { amount: soldAmount } = ReduceData(total_sold)

    const calcPL = Math.floor(((soldAmount - totalInvestment) / totalInvestment) * 100)
    if (Math.sign(calcPL) == 1) {
      profit = calcPL
    }
    if (Math.sign(calcPL) == -1) {
      loss = Math.abs(calcPL)
    }
    return [
      { title: 'Total Units', total: totalUnits },
      { title: 'Total Investment', total: totalInvestment },
      { title: 'Sold Amount', total: soldAmount },
      type === 'main' ? { title: 'Current Amount', total: userData && userData.amount ? userData.amount : 0 } : {},
      { title: 'Profit', total: profit == 100 ? 0 : profit ? profit : 0 },
      { title: 'Loss', total: loss == 100 ? 0 : loss ? loss : 0 },
    ]

  } else if (total_investment.length > 0 && total_sold.length === 0) {
    let { amount: totalInvestment, quantity: totalUnits } = ReduceData(total_investment)
    const soldAmount = 0
    const calcPL = Math.floor(((soldAmount - totalInvestment) / totalInvestment) * 100)
    if (Math.sign(calcPL) == 1) {
      profit = calcPL
    }
    if (Math.sign(calcPL) == -1) {
      loss = Math.abs(calcPL)
    }
    return [
      { title: 'Total Units', total: totalUnits },
      { title: 'Total Investment', total: totalInvestment },
      { title: 'Sold Amount', total: soldAmount },
      type === 'main' ? { title: 'Current Amount', total: userData && userData.amount ? userData.amount : 0 } : {},
      { title: 'Profit', total: profit == 100 ? 0 : profit ? profit : 0 },
      { title: 'Loss', total: loss == 100 ? 0 : loss ? loss : 0 },]
  } else {
    return [
      { title: 'Total Units', total: 0 },
      { title: 'Total Investment', total: 0 },
      { title: 'Sold Amount', total: 0 },
      type === 'main' ? { title: 'Current Amount', total: userData && userData.amount ? userData.amount : 0 } : {},
      { title: 'Profit', total: 0 },
      { title: 'Loss', total: 0 },
    ]
  }
}


export const showData = async (id: string): Promise<any> => {
  const invested = await InvestedModel.find({ user: id })
  return await FilterData(invested, id, 'main')
}


export const list = async (id: string) => {
  const stocks = await StockModel.find({})
  let result: any = []


  for (const el of stocks) {
    const investedData: any = await InvestedModel.find({ stock: el._id, user: id }).populate({ path: 'stock', select: 'name' })
    if (investedData.length > 0) {
      const filteredData = await FilterData(investedData, id, '')
      result.push({
        name: investedData[0].stock.name,
        data: filteredData
      })
    }
  }
  return result

}