import { NextFunction, Request, Response } from "express"
import * as services from './services'
import { IUserRequest } from '../invested/controller';



export const show = async (req: IUserRequest, res: Response, next: NextFunction) => {
  try {
    if (req.user) {
      const data = await services.showData(req.user.id)
      res.status(201).json(data)
    }
  } catch (e) { next(e) }
}


export const list = async (req: IUserRequest, res: Response, next: NextFunction) => {
  try {
    if (req.user) {
      const data = await services.list(req.user.id)
      res.status(201).json(data)
    }
  } catch (e) { next(e) }
}
