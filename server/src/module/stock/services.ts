import { IStock } from './interface';
import StockModel from './model';

export const create = async (stock: IStock): Promise<IStock> => await new StockModel(stock).save()

export const listAll = async (): Promise<IStock[]> => await StockModel.find()


export const update = async (id: string, data: IStock): Promise<void> => {
  await StockModel.findByIdAndUpdate(id, data)
}

export const remove = async (id: string): Promise<void> => {
  await StockModel.findByIdAndRemove(id)
}
