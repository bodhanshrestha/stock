import { IStock } from './interface';
import { NextFunction, Request, Response } from "express"
import * as services from './services'



export const create = async (req: Request, res: Response, next: NextFunction) => {
  try {
    await services.create(req.body)
    res.status(201).json({ message: 'Stock Created Successfully' })
  } catch (e) { next(e) }
}

export const list = async (req: Request, res: Response, next: NextFunction) => {
  try {
    const stocks: IStock[] = await services.listAll()
    res.status(201).json(stocks)
  } catch (e) { next(e) }
}

export const update = async (req: Request, res: Response, next: NextFunction) => {
  const stockId = req.params.id
  try {
    await services.update(stockId, req.body)
    res.status(201).json({ message: 'Stock Updated Successfully' })
  } catch (e) { next(e) }
}
export const remove = async (req: Request, res: Response, next: NextFunction) => {
  const stockId = req.params.id
  try {
    await services.remove(stockId)
    res.status(201).json({ message: 'Stock Deleted Successfully' })
  } catch (e) { next(e) }
}