
import { IStock } from './interface'
import { model, Schema, Model } from 'mongoose';


const stockSchema: Schema = new Schema({
  name: {
    type: String,
    required: [true, 'Please Enter Stock Name'],
  },



}, { timestamps: true })

const StockModel: Model<IStock> = model('stocks', stockSchema)
export default StockModel;