import express from 'express'
import { create, list, update, remove } from './controller'
const router = express.Router()


router.route('/create').post(create)
router.route('/list').get(list)
router.route('/:id/update').put(update)
router.route('/:id/delete').delete(remove)



export default router