import { Document } from 'mongoose';
export interface IUser extends Document {
  name: string;
  email: string;
  username: string;
  password: string;
  address: string;
  phone_number: string;
  amount: number;
  role: string
}