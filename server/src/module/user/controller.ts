import { IUser } from './interface';
import { NextFunction, Request, Response } from "express"
import * as services from './services'


export const register = async (req: Request, res: Response, next: NextFunction) => {
  try {
    const oldUser: IUser | null = await services.checkExistingEmailOrUsername(req.body.username, req.body.email);
    if (oldUser) throw new Error("User Already Exists")
    const user = await services.create(req.body)


    res.status(201).json({
      username: user.username,
      email: user.email,
      message: 'User Registration Successful'
    })
  } catch (e) { next(e) }
}



export const login = async (req: Request, res: Response, next: NextFunction) => {
  try {
    const { user, token } = await services.login(req.body)
    res.status(201).json({
      message: 'User Login Successfully',
      user,
      token
    })
  } catch (e) { next(e) }
}