import { IUser } from './interface';
import UserModel from './model';
import bcrypt from 'bcrypt'
import *  as jwt from 'jsonwebtoken'

var saltRounds = 10;

const createToken = (data: IUser): string => {
  const secret: any = process.env.ACCESS_TOKEN_SECRET
  let token = jwt.sign(
    { username: data.username, id: data._id },
    secret,
    { expiresIn: process.env.JWT_EXPIRE_DATE }
  );
  return token;
}
const hashPassword = (password: string): string => {
  const salt = bcrypt.genSaltSync(saltRounds);
  const hash = bcrypt.hashSync(password, salt);
  return hash
}

export const create = async (user: IUser): Promise<IUser> => {
  user.password = hashPassword(user.password);
  return await new UserModel(user).save()
};

export const checkExistingEmailOrUsername = async (username: string, email: string): Promise<IUser | null> => await UserModel.findOne({
  $or: [{ username: username }, { email: email }],
});


export const login = async (data: any) => {
  return await UserModel.findOne({ username: data.username }).select("+password").then((user: IUser | null) => {
    if (!user) throw new Error("User Not Found")
    const result: boolean = bcrypt.compareSync(data.password, user.password);
    if (!result) throw new Error("Invalid Credentials")

    let token = createToken(user);
    return {
      user: {
        username: user.username,
        email: user.email,
        role: user.role
      },
      token
    }

  })

}
