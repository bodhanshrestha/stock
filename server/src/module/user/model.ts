
import { IUser } from './interface'
import { model, Schema, Model } from 'mongoose';


const userSchema: Schema = new Schema({
  name: {
    type: String,
    required: [true, 'Please Enter Your Name'],

  },
  username: {
    type: String,
    unique: true,
    sparse: true,
    required: [true, "Please Enter Your Username"],
    lowercase: true,
    trim: true,
  },
  email: {
    type: String,
    required: [true, 'Please Enter Your Email'],
    sparse: true,
    unique: true,
  },
  password: {
    type: String,
    required: [true, 'Please Enter Your Password'],
    select: false
  },
  address:
  {
    type: String
  },
  phone_number: {
    type: String,
  },
  role: {
    type: String,
    enum: ['admin', 'member'],
    default: 'member'
  },
  amount: {
    type: Number,
    default: 15000
  }

}, { timestamps: true })

const UserModel: Model<IUser> = model('users', userSchema)
export default UserModel;