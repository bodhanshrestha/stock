import { IInvested } from './interface';
import { NextFunction, Request, Response } from "express"
import * as services from './services'

export interface IUserRequest extends Request {
  user?: {
    id: string,
    username: string
  };
}

export const create = async (req: IUserRequest, res: Response, next: NextFunction) => {
  try {
    if (req.user) {
      await services.create(req.body, req.user.id)
      res.status(201).json({ message: 'Invested Successful' })
    }
  } catch (e) { next(e) }
}

export const get = async (req: IUserRequest, res: Response, next: NextFunction) => {
  const stockId: any = req.query.id
  try {
    if (req.user && stockId) {
      const data = await services.get(stockId, req.user.id)
      res.status(201).json(data)
    }
  } catch (e: any) {
    if (e.message === 'Reduce of empty array with no initial value') {
      e.message = 'You Need To Buy a Stock To Sell'
    }
    next(e)
  }
}

export const list = async (req: IUserRequest, res: Response, next: NextFunction) => {

  try {
    if (req.user) {
      const data = await services.list(req.user.id)
      res.status(201).json(data)
    }
  } catch (e) { next(e) }
}


