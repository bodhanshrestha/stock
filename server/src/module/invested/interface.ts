import { Document } from 'mongoose';
export interface IInvested extends Document {
  stock: string;
  transaction_type: string;
  quantity: number;
  amount: number;
  transaction_date: string;
  user: string
}
