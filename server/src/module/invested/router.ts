import express from 'express'
import { authenticateToken } from '../../middleware/authenticateToken'
import { create, get, list } from './controller'
const router = express.Router()


router.route('/create').post(authenticateToken, create)
router.route('/get/quantity').get(authenticateToken, get)
router.route('/list').get(authenticateToken, list)




export default router