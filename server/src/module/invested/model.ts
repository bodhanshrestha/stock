
import { IInvested } from './interface'
import { model, Schema, Model } from 'mongoose';


const investedSchema: Schema = new Schema({
  stock: {
    type: Schema.Types.ObjectId,
    ref: 'stocks',
    required: true,
  },
  transaction_type: {
    type: String,
    enum: ['buy', 'sell'],
    required: [true, "Please Enter Transaction Type"],
  },
  quantity: {
    type: Number,
    required: [true, 'Please Enter Stock Quantity'],
    min: 0
  },
  amount: {
    type: Number,
    required: [true, 'Please Enter Stock Amount'],
  },
  transaction_date: {
    type: Date,
    required: [true, 'Please Enter Stock Transaction Date'],
  },
  user: {
    type: Schema.Types.ObjectId,
    ref: 'users',
    required: true,
  }

}, { timestamps: true })

const InvestedModel: Model<IInvested> = model('invested', investedSchema)
export default InvestedModel;