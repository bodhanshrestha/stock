import { IInvested } from './interface';
import InvestedModel from './model';

import UserModel from '../user/model'
import { ReduceData } from '../dashboard/services';
export const create = async (invested: IInvested, userId: string): Promise<void> => {
  const user = await UserModel.findOne({ _id: userId })

  if (user) {
    if (invested.transaction_type === 'buy') {
      const dec = user.amount - invested.amount
      await UserModel.findByIdAndUpdate(userId, { amount: dec })
    }
    if (invested.transaction_type === 'sell') {
      const inc = user.amount + invested.amount
      await UserModel.findByIdAndUpdate(userId, { amount: inc })
    }

  }
  await new InvestedModel({ ...invested, user: userId }).save()
  return

}



export const get = async (stockId: string, userId: string) => {
  const invested = await InvestedModel.find({ stock: stockId, user: userId })
  const buy = invested.filter((el) => el.transaction_type === 'buy')
  const sell = invested.filter((el) => el.transaction_type === 'sell')
  if (buy.length > 0 && sell.length > 0) {
    const { quantity: buyQuantity } = ReduceData(buy)
    const { quantity: sellQuantity } = ReduceData(sell)
    const leftQuantity = buyQuantity - sellQuantity
    return leftQuantity
  } else {
    const { quantity: buyQuantity } = ReduceData(buy)
    return buyQuantity
  }

}
export const list = async (userId: string) => {
  return await InvestedModel.find({ user: userId }).populate({ path: 'stock', select: 'name' })
}