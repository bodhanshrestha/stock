import mongoose from 'mongoose'

const connectDB = async () => {
  try {
    const db: any = process.env.DATABASE?.toString()
    const con = await mongoose.connect(db, {});
    console.log(`Database connected : ${con.connection.host}`)
  } catch (error: any) {
    console.error(`Error: ${error.message}`)
    process.exit(1)
  }
}

export default connectDB